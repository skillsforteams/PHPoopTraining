<?php 
namespace PHPoopTraining\Patterns\Adapter;
use PHPoopTraining\Elephant\BluePhplephant;
use PHPoopTraining\Horse\Horse;

class HorseElephantAdapter implements Horse {
    
    private BluePhplephant $elephant; 

    public function __construct(BluePhplephant $elephant) {
        $this->elephant = $elephant;
    }
    
    public function ride():string {
        return $this->elephant->ride();
    }
    public function whinny(): string {
        return "Töööörööööö i am blue";
    }
    public function eat(): string {
        return "The only thing i eat is php code!";
    }
    
}