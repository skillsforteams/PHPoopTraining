<?php
namespace PHPoopTraining\Horse;


class StdoutChip implements Chip {
    public function track(string $message): void {
        echo $message;
    }
}