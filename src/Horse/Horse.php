<?php 
namespace PHPoopTraining\Horse;

// microsoft standard naming would be IHorse
interface Horse { 

    public function ride(): string;
    public function eat(): string;
    public function whinny(): string;


}