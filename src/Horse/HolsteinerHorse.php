<?php 
namespace PHPoopTraining\Horse;

class HolsteinerHorse implements Horse { 

    public function ride(): string {
        return "ohh yeah i ride through the fields";
    }

    public function eat(): string {
        return "yummy give me wheat";        
    }

    public function whinny(): string {
        return "whinny whinny i am exited";
    }
}