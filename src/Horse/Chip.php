<?php
namespace PHPoopTraining\Horse;

interface Chip {
    public function track(string $message): void;
}