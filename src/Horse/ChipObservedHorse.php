<?php 
namespace PHPoopTraining\Horse;

class ChipObservedHorse extends HolsteinerHorse { 
    
    private array $chips = [];
    
    /** This should show the observer pattern */
    public function addChip(Chip $chip) {
        $this->chips[] = $chip;
    }

    private function message(string $message):void {
        foreach($this->chips as $chip) {
            $chip->track($message);
        }
    }

    public function ride(): string {
        $result = parent::ride();
        $this->message($result);
        return $result;
        
    }

    public function eat(): string {
        $result = parent::eat();
        $this->message($result);
        return $result;     
    }

    public function whinny(): string {
        $result = parent::whinny();
        $this->message($result);
        return $result;     
    }
}