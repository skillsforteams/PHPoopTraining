<?php 
namespace PHPoopTraining\BMRcalc; 

class BMRcalc {

    public function getCalculator($gender): Calculator
    {
        switch($gender){
            case "m": return new MaleCalculator();
            case "f": return new FemaleCalculator();
        }

    }
    
    public function calc($gender,$age,$sizecm,$weight){
       $calculator = $this->getCalculator($gender);    
       $result = $calculator->calc($age,$sizecm,$weight);
       return floor($result);
    }
}
    