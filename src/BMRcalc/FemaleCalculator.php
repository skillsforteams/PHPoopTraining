<?php 
namespace PHPoopTraining\BMRcalc; 

class FemaleCalculator implements Calculator {

    public function calc(int $age, int $sizecm, int $weight) :float {
        $result = 655.1 + (9.6 * $weight) + (1.8 * $sizecm) - (4.7 * $age);
        return $result;
    }
} 
