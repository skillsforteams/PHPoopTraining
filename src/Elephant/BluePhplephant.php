<?php
namespace PHPoopTraining\Elephant;

use PHPoopTraining\Elephant\Elephant;
use PHPoopTraining\Elephant\PhpProgrammer;

class BluePhplephant extends Phplephant {
   
    public $color = "blue";

    /**
     * overwrites Elephant::sleep() function
     */
    public function sleep() {
        return "i sleep on the back";
    }

    public function standOnHindLegs() {
        return "yieehaa i stand on my hind legs";
    }

    public function ride() {
        return "ohh i cool i am beeing ridden";
    }

}