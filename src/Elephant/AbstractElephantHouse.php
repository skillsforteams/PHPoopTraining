<?php
namespace PHPoopTraining\Elephant;

abstract class AbstractElephantHouse {
    
    public Array $elephants;
    
    public function addElephant(Elephant $elephant) {
        $this->elephants[] = $elephant;
    }

    abstract function showAllElephants();
}