<?php
namespace PHPoopTraining\Elephant;

use PHPoopTraining\Elephant\Elephant;
use PHPoopTraining\Elephant\PhpProgrammer;

class Phplephant extends Elephant implements PhpProgrammer {
   
    public $phplogo ="PHP";
    public $cuddlePower = 25;
    public $phpAddiction = 100;

    public function codePHP() {
        return "i code php";
    }
    public function learnPHP() {
        return "i learn php";
    }
    public function cuddle(Phplephant $partner) {
        return $this->name." cuddles his elephant friend ".$partner->name; 
    }
    public function pairProgramming(PhpProgrammer $programmer) {
        return "my code partner says:".$programmer->codePHP(); 
    }

}