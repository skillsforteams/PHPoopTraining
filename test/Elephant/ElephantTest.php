<?php
namespace PHPoopTraining\Test\Elephant;
use PHPUnit\Framework\TestCase;
use PHPoopTraining\Elephant\Elephant;

class ElephantTest extends TestCase
{
  public $testElephantName = "Benjamin";

  public function getTestElephant() {
    return new Elephant($this->testElephantName);
  }
/*
 public function testSimpleConstruct() {
    
    $myElephantPeter = new Elephant("peter");
    echo $myElephantPeter->run();
    if ($myElephantPeter->name == "peter") {
      echo "name korrekt gesetzt";
    }
    $myElephantPaul = new Elephant("paul");
    $myElephantMarry = new Elephant("Marry");
 }*/


 public function testConstruct() {
   $elephant = $this->getTestElephant();   
   $this->assertInstanceOf('PHPoopTraining\Elephant\Elephant', $elephant);
 }  
 public function testRun() {
    $elephant = $this->getTestElephant();
    $this->assertEquals("i am running arround", $elephant->run());
 }
 public function testSleep() {
      $elephant = $this->getTestElephant();
      $this->assertEquals("is sleeping on the side", $elephant->sleep());
 }
 public function testToroe() {
    $elephant = $this->getTestElephant();
    $this->assertEquals("Töööörööööö i am ".$this->testElephantName, $elephant->toroe());
 }
 public function stamp() {
    $elephant = $this->getTestElephant();
    $this->assertEquals("stamp stamp arround", $elephant->stamp());
 }

}