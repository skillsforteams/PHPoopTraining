<?php
namespace PHPoopTraining\Elephant\Test;
use PHPUnit\Framework\TestCase;
use PHPoopTraining\Elephant\RedPhplephant;
use PHPoopTraining\Test\Elephant\PhplephantTest;

class RedPhplephantTest extends PhplephantTest 
{
    public $testElephantName = "Red";
    
    public function getTestElephant() {
        return new RedPhplephant(); 
    }
    public function testConstruct() {
        $elephant = $this->getTestElephant();
        $this->assertInstanceOf("PHPoopTraining\Elephant\RedPhplephant", $elephant);
    }

    public function testRide() {
        $elephant = $this->getTestElephant();
        $this->assertEquals("I will not let him ride i throw him", $elephant->ride());
    }

    public function testStandOnHead() {
        $elephant = $this->getTestElephant();
        $this->assertEquals("Look i am standing on my head", $elephant->testStandOnHead());
    }


  

}