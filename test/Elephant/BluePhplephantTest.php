<?php
namespace PHPoopTraining\Test\Elephant;
use PHPUnit\Framework\TestCase;
use PHPoopTraining\Elephant\BluePhplephant;
use PHPoopTraining\Test\Elephant\PhplephantTest;

class BluePhplephantTest extends PhplephantTest
{ 
  public $testElephantName = "Blue";

  public function getTestElephant() {
    return new BluePhplephant($this->testElephantName);
  }

 public function testConstruct() {
   $elephant = $this->getTestElephant();  
   $this->assertInstanceOf('PHPoopTraining\Elephant\BluePhplephant', $elephant);
 }  
 
 public function testSleep(){
  $elephant = $this->getTestElephant(); 
  $this->assertEquals("i sleep on the back", $elephant->sleep());
 }

 public function testStandOnHindLegs(){
  $elephant = $this->getTestElephant(); 
  $this->assertEquals("yieehaa i stand on my hind legs", $elephant->standOnHindLegs());
 }

 public function testRide() {
   $elephant = $this->getTestElephant();
   $this->assertEquals("ohh i cool i am beeing ridden", $elephant->ride());
 }


}