<?php
namespace PHPoopTraining\Test\Elephant;
use PHPUnit\Framework\TestCase;
use PHPoopTraining\Elephant\Phplephant;
use PHPoopTraining\Test\Elephant\ElephantTest;

class PhplephantTest extends ElephantTest
{ 
  public $testElephantName = "Rasmus";

  public function getTestElephant() {
    return new Phplephant($this->testElephantName);
  }

public function testConstruct() {
  $elephant = $this->getTestElephant();  
  $this->assertInstanceOf('PHPoopTraining\Elephant\\Phplephant', $elephant);
}
  
 
public function codePHP() {
  $elephant = $this->getTestElephant();
  $this->assertEquals("i code php", $elephant->codePHP());;
}
public function learnPHP() {
  $elephant = $this->getTestElephant();
  $this->assertEquals("i learn php", $elephant->learnPHP());;
}
public function testCuddle() {
  $elephant = $this->getTestElephant();
  $partnerElephant = new Phplephant("Arne");
  $this->assertEquals($this->testElephantName." cuddles his elephant friend Arne", $elephant->cuddle($partnerElephant));;
}

public function testPairProgramming() {
  $elephant = $this->getTestElephant();
  $partnerElephant = new Phplephant("Zeev");
  $this->assertEquals("my code partner says:i code php", $elephant->pairProgramming($partnerElephant));
}

}