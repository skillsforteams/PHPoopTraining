<?php 
namespace PHPoopTraining\Test\Horse; 
use PHPoopTraining\Horse\Horse;
use PHPoopTraining\Horse\HolsteinerHorse;
use PHPUnit\Framework\TestCase;

class HolsteinerHorseTest extends TestCase {

    private function getTestObject(): Horse {
        $horse = new HolsteinerHorse();
        return $horse; 
    } 

    public function testEat() {
        $this->assertEquals(
            "yummy give me wheat",
            $this->getTestObject()->eat()
            );
    }
    
    public function testRide() {
        $this->assertEquals(
            "ohh yeah i ride through the fields",
             $this->getTestObject()->ride()
            );
    }

    public function testWhinny() {
        $this->assertEquals(
            "whinny whinny i am exited",
            $this->getTestObject()->whinny()
        );
    }
}