<?php 
namespace PHPoopTraining\Test\Horse; 
use PHPoopTraining\Horse\Horse;
use PHPoopTraining\Horse\ChipObservedHorse;
use PHPoopTraining\Horse\StdoutChip;
use PHPUnit\Framework\TestCase;

class ChipObservedHorseTest extends TestCase {

    private function getTestObject(): Horse {
        $horse = new ChipObservedHorse();
        return $horse; 
    } 

    public function testAddChip(){
        $horse = $this->getTestObject();
        $chip = new StdoutChip();
        $horse->addChip($chip);
        $this->expectOutputString("yummy give me wheat");
        $horse->eat();

    }

    public function testEat() {
        $this->assertEquals(
            "yummy give me wheat",
            $this->getTestObject()->eat()
            );
    }
    
    public function testRide() {
        $this->assertEquals(
            "ohh yeah i ride through the fields",
             $this->getTestObject()->ride()
            );
    }

    public function testWhinny() {
        $this->assertEquals(
            "whinny whinny i am exited",
            $this->getTestObject()->whinny()
        );
    }
}