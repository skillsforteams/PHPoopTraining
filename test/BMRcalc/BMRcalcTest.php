<?php
namespace PHPoopTraining\Test\BMRcalc;
use PHPUnit\Framework\TestCase;
use PHPoopTraining\BMRcalc\BMRcalc;

class BMRcalcTest extends TestCase {

    public function testClassExists() {
        $bmrcalc = new BMRcalc();
        $this->assertInstanceOf("PHPoopTraining\BMRcalc\BMRcalc", $bmrcalc);
    }

    public function providerBMRdataArray() 
    {
        $data[] = [
        "person"=>[
            "gender"=>"m",
            "weight"=>100,
            "age"=>30,
            "sizecm"=>185,
            "active"=>0        
        ],
        "result"=>2157
        ];
    $data[] = [
        "person"=>[
            "age" => 18,
            "gender"=>"m", 
            "weight"=>80,
            "sizecm"=>180,
            "active"=>0           
        ],
        "result"=>1940];
     $data[] = [
           "person"=>[
               "age" => 30,
                "gender"=>"f", 
                "weight"=>65,
                "sizecm"=>165,
                "active"=>0           
            ],
            "result"=>1435];    
    return $data;
    }

    /**
    * @dataProvider providerBMRdataArray 
    */
    public function testCalc($person,$expected){
        $bmrcalc = new BMRcalc();
        
        $result = $bmrcalc->calc($person["gender"], $person["age"],$person["sizecm"],$person["weight"]);
        $this->assertEquals($expected, $result);
    }

}