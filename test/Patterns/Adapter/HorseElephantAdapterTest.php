<?php 
namespace PHPoopTraining\Test\Patterns\Adapter;
use PHPUnit\Framework\TestCase;
use PHPoopTraining\Horse\HolsteinerHorse; 
use PHPoopTraining\Horse\Horse;
use PHPoopTraining\Elephant\BluePhplephant;
use PHPoopTraining\Patterns\Adapter\HorseElephantAdapter;

class HorseElephantAdapterTest extends TestCase {

    public function getTestObject(): \PHPoopTraining\Horse\Horse {
        $elephant = new \PHPoopTraining\Elephant\BluePhplephant("blue");
        $horseElephantAdapter = new HorseElephantAdapter($elephant); 
        return $horseElephantAdapter;
    }

    public function testRide() {
        $this->assertEquals(
            "ohh i cool i am beeing ridden",
            $this->getTestObject()->ride()
        );
    }
 
    public function testWhinny() {
        $this->assertEquals(
            "Töööörööööö i am blue",
            $this->getTestObject()->whinny()
        );
    }

    public function testEat() {
        $this->assertEquals(
            "The only thing i eat is php code!",
            $this->getTestObject()->eat()
        );
    }
   
}